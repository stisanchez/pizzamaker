package solucionesglobales.pizzeria.pizza;

/**
 * Created by s_san on 12/10/2017.
 */

public class PizzaJamonQueso implements IPizza {
    private String mName;
    private String[] mIngredients = {"Jamon", "Queso", "Hongos"};

    public PizzaJamonQueso(Builder pBuilder){
        mName = pBuilder.mName;
    }

    public static class Builder{
        private final String mName;
        private final String [] mIngredients;

        public Builder(String pName, String [] pIngredients){
            this.mName = pName;
            this.mIngredients = pIngredients;
        }
    }

    /**
     * get the pizza's ingredients
     * @return
     */
    @Override
    public String[] getIngredients(){
        return mIngredients;
    }

    /**
     * return a message with the name
     * @return
     */
    @Override
    public String prepare() {
        return "Su pizza" + this.mName +" estara en 20 minutos";
    }

}
