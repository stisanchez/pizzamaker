package solucionesglobales.pizzeria.pizza;

/**
 * Created by s_san on 12/10/2017.
 */

public interface IPizzaFactory  {
    enum PizzaType{
        SUPREMA, JAMON_QUESO;
    }

    IPizza getPizza(PizzaType pPizza);
}
