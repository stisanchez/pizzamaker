package solucionesglobales.pizzeria.pizza;

/**
 * Created by s_san on 12/10/2017.
 */

public class PizzaSuprema implements IPizza {
    private String mName;
    private String[] mIngredients;

    public PizzaSuprema(Builder pBuilder) {
        mName = pBuilder.mName;
        mIngredients = pBuilder.mIngredients;
    }

    /**
     * get the pizza's ingredients
     * @return
     */
    @Override
    public String[] getIngredients() {
        return mIngredients;
    }

    /**
     * return a message with the name
     * @return
     */
    @Override
    public String prepare() {
        return "Su pizza " + this.mName + " estara en 40 minutos";
    }

    public static class Builder {
        private final String mName;
        private final String[] mIngredients;
        public Builder(String pName, String[] pIngredients) {
            this.mName = pName;
            this.mIngredients = pIngredients;
        }
    }


}
