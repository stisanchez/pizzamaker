package solucionesglobales.pizzeria.pizza;

/**
 * Created by s_san on 12/10/2017.
 */

public class ApplicationPizzaFactory implements IPizzaFactory {

    @Override
    public IPizza getPizza(PizzaType pPizza){
        IPizza pizza;


        switch (pPizza) {
            case SUPREMA:
                String [] ingredientsS = {"Cebolla","Chile","Queso","Salame","Peperone"};
                pizza = new PizzaSuprema(new PizzaSuprema.Builder("Suprema",ingredientsS));
                break;
            case JAMON_QUESO:
                String [] ingredientsJQ = {"Queso","Jamon","Salsa Tomate"};
                pizza = new PizzaJamonQueso(new PizzaJamonQueso.Builder("Jamon & Hongos",ingredientsJQ));
                break;
            default:
                String [] ingredients = {"Cebolla","Chile","Queso","Salame","Peperone"};
                pizza = new PizzaSuprema(new PizzaSuprema.Builder("Suprema", ingredients));
                break;
        }
        return pizza;
    }

}
