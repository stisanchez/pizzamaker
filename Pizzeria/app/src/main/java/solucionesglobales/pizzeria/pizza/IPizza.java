package solucionesglobales.pizzeria.pizza;

/**
 * Created by s_san on 12/10/2017.
 */

public interface IPizza {
    String [] getIngredients();
    String prepare();
}
