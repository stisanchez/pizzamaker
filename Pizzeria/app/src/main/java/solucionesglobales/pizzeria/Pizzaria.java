package solucionesglobales.pizzeria;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import solucionesglobales.pizzeria.pizza.ApplicationPizzaFactory;
import solucionesglobales.pizzeria.pizza.IPizza;
import solucionesglobales.pizzeria.pizza.IPizzaFactory;

public class Pizzaria extends AppCompatActivity {

    Button btnPreparar;
    Spinner spnPizzas;
    TextView txtIngredients;
    private String[] arraySpinner = new String[] {"SUPREMA", "QUESO & JAMON"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pizzaria);


        spnPizzas = (Spinner) findViewById(R.id.ActivityPizzeria_spnPizzas);
        btnPreparar = (Button)findViewById(R.id.ActivityPizzeria_btn_Preparar);
        txtIngredients =  (TextView) findViewById(R.id.ActivityPizzeria_txtIngredients);

        btnPreparar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preparePizza();
            }
        });

        spnPizzas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                getIngredients(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraySpinner);
        spnPizzas.setAdapter(adapter);
    }


    private void preparePizza(){
        IPizzaFactory pizzaFactory = new ApplicationPizzaFactory();
        IPizza pizza;
        if(spnPizzas.getSelectedItemPosition() == 0){
            pizza = pizzaFactory.getPizza(IPizzaFactory.PizzaType.SUPREMA);
        }else{
            pizza = pizzaFactory.getPizza(IPizzaFactory.PizzaType.JAMON_QUESO);
        }
        Toast.makeText(this, pizza.prepare(), Toast.LENGTH_LONG).show();
    }

    private void getIngredients(int pPosition){
        IPizzaFactory pizzaFactory = new ApplicationPizzaFactory();
        IPizza pizza;
        txtIngredients.setText("");
        if (pPosition == 0){
            pizza = pizzaFactory.getPizza(IPizzaFactory.PizzaType.SUPREMA);
        }else{
            pizza = pizzaFactory.getPizza(IPizzaFactory.PizzaType.JAMON_QUESO);
        }

        String [] ingredientsPizza = pizza.getIngredients();

        int quantity = ingredientsPizza.length;
        for(int i = 0; i < quantity; i++) {
            txtIngredients.append(ingredientsPizza[i] + ", ");
        }
    }

}
